macroScript ResetTransforms
	category:"#Rigging"
    buttonText:"RESET"
    toolTip:"Reset Transforms"
(
	for controller in getCurrentSelection() do
	(
		controller_parent = controller.parent
		
		if controller_parent != undefined then
		(
			controller.transform = controller_parent.transform * matrix3 1
		)
		else
		(
			controller.transform = matrix3 1
		)
	)
)