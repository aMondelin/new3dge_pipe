macroScript CreateZero
	category:"#Rigging"
    buttonText:"ZERO"
    toolTip:"Create Zero"
(
	separator = "_"
	zero_type_name = "HZ"
	
	selected_objects = getCurrentSelection()
	if selected_objects.count != 0 do (		
		for object_ in selected_objects do (
			object_name_filter = filterString object_.name separator
			
			if object_name_filter.count == 4 or object_name_filter.count == 5 then
			(
				object_name_filter[2] = zero_type_name
			)
			else
			(
				append object_name_filter zero_type_name
			)
			
			zero_object_name = object_name_filter[1]
			for object_name in object_name_filter do
			(
				if object_name != zero_object_name do
				(
					zero_object_name += separator + object_name
				)
			)
			
			zero_object = Point size:.1 wirecolor:green name:zero_object_name box:true cross:false axistripod:false centermarker:false
			zero_object.transform = object_.transform
			zero_object.parent = object_.parent
			object_.parent = zero_object
		)
	)
)