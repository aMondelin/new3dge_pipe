set MAX_VERSION=2023
call %~dp0\config\newedge_env.bat


set INI_SOURCE=%localappdata%\Autodesk\3dsMax\%MAX_VERSION% - 64bit\ENU\3dsmax.ini
set INI_TARGET=%localappdata%\Autodesk\3dsMax\%MAX_VERSION% - 64bit\ENU\%MAX_NAME%_3dsmax.ini
set INI_USER_SOURCE=%NEWEDGE_MAX_ROOT%\config\Plugin.UserSettings.ini
set INI_USER_TARGET=%LOCALAPPDATA%\Autodesk\3dsMax\%MAX_VERSION% - 64bit\enu\%MAX_NAME%_Plugin_UserSettings.ini


powershell -command "If (!(Test-Path '%INI_TARGET%')) {Copy-Item -Path '%INI_SOURCE%' -Destination '%INI_TARGET%'; };" ^
 "get-content '%INI_USER_SOURCE%' | foreach { [System.Environment]::ExpandEnvironmentVariables($_) } | set-content -path '%INI_USER_TARGET%';" ^
 "$content = Get-Content -path '%INI_TARGET%'; $content -Replace '(Additional Icons=.*)', 'Additional Icons=%NEWEDGE_MAX_ROOT%' | Out-File '%INI_TARGET%';" ^
 "$content = Get-Content -path '%INI_TARGET%'; $content -Replace '(Startup Scripts=.*)', 'Startup Scripts=%NEWEDGE_MAX_ROOT%\maxscript\startupscripts' | Out-File '%INI_TARGET%';"


start "3DS Max" "%MAX_DIRECTORY%\3dsmax.exe" -p %MAX_NAME%_Plugin_UserSettings.ini %* -i %MAX_NAME%_3dsmax.ini

exit
