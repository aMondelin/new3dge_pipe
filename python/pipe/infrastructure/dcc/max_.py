import os

from pymxs import runtime as mrt
from pipe.infrastructure.dcc.abstract import AbstractDcc


class Max(AbstractDcc):
    extension = 'max'

    # SCENE

    def get_dir_path(self):
        return mrt.maxFilePath

    def save_file(self, file_path: str):
        mrt.savemaxfile(file_path, quiet=True)

    def hold_scene(self):
        mrt.holdMaxFile()

    def fetch_scene(self):
        mrt.fetchMaxFile(quiet=True)

    # LAYERS

    def layer_count(self):
        return mrt.LayerManager.count

    def set_default_layer_active(self):
        mrt.LayerManager.getLayer(0).current = True

    def is_default_layer(self, layer_index: int):
        return mrt.LayerManager.getLayer(layer_index) == mrt.LayerManager.getLayer(0)

    def is_empty_layer(self, layer_index: int):
        layer = mrt.LayerManager.getLayer(layer_index)
        return not layer.getNumNodes()

    def delete_layer(self, layer_index: int):
        layer = mrt.LayerManager.getLayer(layer_index)
        mrt.LayerManager.deleteLayerByName(layer.name)

    def move_objects_to_default_layer(self, dcc_objects: list):
        default_layer = mrt.LayerManager.getLayer(0)

        for dcc_object in dcc_objects:
            default_layer.addNode(dcc_object)

    # VIEWPORT

    def disable_viewport(self):
        mrt.disableSceneRedraw()

    def enable_viewport(self):
        mrt.enableSceneRedraw()
        mrt.completeRedraw()

    # OBJECTS

    def return_scene_objects(self, asset_object):
        return [asset_object(object_name=obj.name, dcc_object=obj) for obj in mrt.objects]

    def get_dcc_object(self, name: str):
        return mrt.getNodeByName(name)

    def rename_object(self, dcc_object, name: str):
        dcc_object.name = name

    def delete_object(self, dcc_object):
        mrt.delete(dcc_object)

    def get_object_transform(self, dcc_object):
        return dcc_object.transform

    def set_object_transform(self, dcc_object, transform):
        dcc_object.transform = transform

    # KEYFRAMES

    def set_animation_range(self, first_frame, last_frame):
        mrt.animationRange = mrt.interval(first_frame, last_frame)

    def set_current_frame(self, frame):
        mrt.execute('sliderTime = {} as time'.format(frame))

    def set_auto_key(self, state: bool):
        if state:
            mrt.execute('set animate on')
        else:
            mrt.execute('set animate off')

    def get_objects_keyframes(self, objects: list):
        objects_keyframes = list()

        tracks_anim = ['position', 'rotation', 'scale']
        for obj in objects:
            for track in tracks_anim:
                keyframes = mrt.execute('$\'{}\'.{}.controller.keys'.format(obj.full_name, track))
                for keyframe in keyframes:
                    objects_keyframes.append(int(keyframe.time))

        return sorted(objects_keyframes)

    def offset_keyframes(self, dcc_object, frame_offset):
        tracks_names = ['position', 'rotation', 'scale']

        for track_name in tracks_names:
            mrt.execute('moveKeys $\'{}\'.{}.controller {}'.format(dcc_object.name, track_name, frame_offset))

    def delete_keyframes(self, dcc_object):
        mrt.deleteKeys(dcc_object)

    # IMPORT

    def import_fbx(self, file_path: str):
        mrt.importFile(file_path, mrt.Name('noPrompt'))

    def export_fbx(self, file_path: str):
        mrt.exportFile(file_path, mrt.Name('noPrompt'))
