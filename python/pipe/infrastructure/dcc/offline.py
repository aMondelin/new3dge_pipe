from dataclasses import dataclass

from pipe.infrastructure.dcc.abstract import AbstractDcc


@dataclass
class AbstractObject:
    name: str = ''
    transform: str = ''


SCENE_OBJECTS = [
    AbstractObject('plop001_C_Root_001', '[0, 0, 0]'),
    AbstractObject('plop001_C_pelvis_001', '[0, 0, 0]'),
    AbstractObject('plop001:Pelvis_001', '[0, 0, 0]'),
    AbstractObject('032#plop001:align_plop001_C_pelvis_001', '[5, 0, 0]'),
    AbstractObject('plop001_mixamorig:Pelvis_001', '[0, 0, 0]'),
    AbstractObject('032#mixamoRig:align_plop001_C_pelvis_001', '[10, 0, 0]'),
    AbstractObject('toto001_C_Root_001', '[0, 0, 0]'),
    AbstractObject('toto001_C_head_001', '[0, 0, 0]'),
    AbstractObject('toto001:Head_001', '[0, 0, 0]'),
    AbstractObject('032#toto001:align_toto001_C_head_001', '[0, 5, 0]'),
    AbstractObject('toto001_mixamorig:Head_001', '[0, 0, 0]'),
    AbstractObject('032#mixamoRig:align_toto001_C_head_001', '[0, 10, 0]'),
    AbstractObject('toto001_C_hand_L_001', '[0, 0, 0]'),
    AbstractObject('toto001:Hand_L_001', '[0, 0, 0]'),
    AbstractObject('032#toto001:align_toto001_C_hand_L_001', '[0, 0, 5]'),
    AbstractObject('toto001_mixamorig:Hand_L_001', '[0, 0, 0]'),
    AbstractObject('032#mixamoRig:align_toto001_C_hand_L_001', '[0, 0, 10]'),
    AbstractObject('toto001_C_head_002', '[0, 0, 0]'),
    AbstractObject('toto001_mixamorig:Head_002', '[0, 0, 0]'),
    AbstractObject('032#mixamoRig:align_toto001_C_head_003', '[0, 0, 30]'),
]


class Offline(AbstractDcc):
    extension = 'offline'

    # OBJECTS

    def return_scene_objects(self, asset_object):
        return [asset_object(object_name=obj.name, dcc_object=obj) for obj in SCENE_OBJECTS]

    def get_dcc_object(self, name: str):
        for obj in SCENE_OBJECTS:
            if obj.name == name:
                return obj

    def rename_object(self, dcc_object, name: str):
        dcc_object.name = name

    def get_object_transform(self, dcc_object):
        return dcc_object.transform

    def set_object_transform(self, dcc_object, transform):
        dcc_object.transform = transform

    # KEYFRAMES

    def get_objects_keyframes(self, objects: list):
        return [2, 8, 10, 16, 25]
