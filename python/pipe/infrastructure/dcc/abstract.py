

class AbstractDcc:
    extension = ''

    # SCENE

    def get_dir_path(self):
        raise NotImplementedError

    def save_file(self, file_path: str):
        raise NotImplementedError

    def hold_scene(self):
        raise NotImplementedError

    def fetch_scene(self):
        raise NotImplementedError

    # LAYERS

    def layer_count(self):
        raise NotImplementedError

    def set_default_layer_active(self):
        raise NotImplementedError

    def is_default_layer(self, layer_index: int):
        raise NotImplementedError

    def is_empty_layer(self, layer_index: int):
        raise NotImplementedError

    def delete_layer(self, layer_index: int):
        raise NotImplementedError

    def move_objects_to_default_layer(self, dcc_objects: list):
        raise NotImplementedError

    # VIEWPORT

    def disable_viewport(self):
        raise NotImplementedError

    def enable_viewport(self):
        raise NotImplementedError

    # OBJECTS

    def return_scene_objects(self, asset_object):
        raise NotImplementedError

    def get_dcc_object(self, name: str):
        raise NotImplementedError

    def rename_object(self, dcc_object, name: str):
        raise NotImplementedError

    def delete_object(self, dcc_object):
        raise NotImplementedError

    def get_object_transform(self, dcc_object):
        raise NotImplementedError

    def set_object_transform(self, dcc_object, transform):
        raise NotImplementedError

    # KEYFRAMES

    def set_animation_range(self, first_frame, last_frame):
        raise NotImplementedError

    def set_current_frame(self, frame):
        raise NotImplementedError

    def set_auto_key(self, state: bool):
        raise NotImplementedError

    def get_objects_keyframes(self, objects: list):
        raise NotImplementedError

    def offset_keyframes(self, dcc_object, frame_offset):
        raise NotImplementedError

    def delete_keyframes(self, dcc_object):
        raise NotImplementedError

    # IMPORT

    def import_fbx(self, file_path: str):
        raise NotImplementedError

    def export_fbx(self, file_path: str):
        raise NotImplementedError
