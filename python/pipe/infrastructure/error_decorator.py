import functools

from tools.message_box import MessageBox


def error_decorator(function):
    @functools.wraps(function)
    def wrapper(*args, **kwargs):
        try:
            return function(*args, **kwargs)

        except Exception as exception:
            message_box = MessageBox(title='Error', text=str(exception))
            message_box.show()

            raise exception

    return wrapper
