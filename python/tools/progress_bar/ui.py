from PySide2.QtCore import Qt
from PySide2.QtGui import QColor, QPalette
from PySide2.QtWidgets import QApplication, QLabel, QProgressBar, QHBoxLayout, QVBoxLayout, QWidget


class ProgressUi(QWidget):

    def __init__(self, parent=None):
        QWidget.__init__(self, parent)

        self.cancelled = False
        self.progress_max_value = None
        self.progress_value = 0

        self.init_ui()

    @staticmethod
    def close_existing_window():
        widgets = QApplication.topLevelWidgets()
        for widget in widgets:
            if widget.inherits('ProgressUi'):
                widget.close()

    def init_ui(self):
        self.setFixedSize(415, 100)

        # Main Layout
        self.qlayout_progress_ui = QVBoxLayout()
        self.setLayout(self.qlayout_progress_ui)
        self.qlayout_progress_ui.setContentsMargins(17, 10, 17, 20)
        self.qlayout_progress_ui.setSpacing(6)

        self.qlabel_progress = QLabel('IN PROGRESS...')
        self.qlayout_progress_ui.addWidget(self.qlabel_progress)
        self.qprogressbar_bar = QProgressBar()
        self.qpalette_progress = QPalette()
        self.qpalette_progress.setColor(QPalette.Highlight, QColor(Qt.blue))
        self.qprogressbar_bar.setPalette(self.qpalette_progress)
        self.qprogressbar_bar.setMinimumWidth(325)
        self.qlayout_progress_ui.addWidget(self.qprogressbar_bar)

        self.qwidget_task_count = QWidget()
        self.qwidget_task_count.setMaximumHeight(13)
        self.qlayout_progress_ui.addWidget(self.qwidget_task_count)
        self.qlayout_task_count = QHBoxLayout(self.qwidget_task_count)
        self.qlabel_task_count = QLabel('STEP: ')
        self.qlabel_task_count.setMaximumWidth(35)
        self.qlayout_task_count.addWidget(self.qlabel_task_count)
        self.qlabel_current_task_count = QLabel()
        self.qlayout_task_count.addWidget(self.qlabel_current_task_count)

    def closeEvent(self, event):
        self.cancelled = True

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Escape:
            self.cancelled = True

    def set_progress_max(self, max: int):
        self.progress_max_value = max
        self.qprogressbar_bar.setMaximum(max)

    def set_progress(self, display_step: str):
        self.progress_value += 1

        self.qprogressbar_bar.setValue(self.progress_value)
        self.qlabel_current_task_count.setText(display_step)

        self.update()

    def set_progress_label(self, text: str):
        self.qlabel_progress.setText(text)

    def set_progress_color_end(self):
        self.qpalette_progress.setColor(QPalette.Highlight, QColor(Qt.green))

    def set_window_title(self, title: str):
        self.setWindowTitle(title)

    def update(self):
        QApplication.processEvents()
