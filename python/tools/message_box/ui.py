from PySide2.QtWidgets import QDialog, QMessageBox, QPushButton


class MessageBox(QMessageBox):

    def __init__(self, title: str = None, text: str = None, buttons: list = []):
        QMessageBox.__init__(self)

        self.set_title(title)
        self.set_text(text)
        self.set_buttons(buttons)

    def set_title(self, title: str):
        if title:
            self.setWindowTitle(title)

    def set_text(self, text: str):
        if text:
            self.setText(text)

    def set_buttons(self, buttons: list):
        if buttons:
            for button_text in buttons:
                button = QPushButton(button_text, QDialog())
                self.addButton(button, QMessageBox.ActionRole)

    def show(self):
        return self.exec_()
