#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os
from PySide2 import QtWidgets

from pipe.infrastructure.error_decorator import error_decorator
from tools.message_box import MessageBox
from tools.mocapy.api import assets_roots, file_path, frame_range, import_animation, mocap_rig, \
    store_mocap_objects, transfer_animation
from tools.mocapy.infrastructure.import_mode import Mixamo, Mobu, Xsense
from tools.mocapy.infrastructure.ports import Asset, Storage, UserInput, ExternalSingleton
from tools.mocapy.ui import MainWindow


MOCAP_TABLE = {
    'Motion Builder': Mobu(),
    'Mixamo': Mixamo(),
    'Xsense': Xsense()
}

ASSETS_DISPLAY = '{name}_{occurrence}'


class MocapyApplication:
    name = 'MOCAPY'
    version = '2.0.1'
    last_released = '2023/10/20'

    def __init__(self, args):
        MainWindow.close_existing_window()

        # self.pyside_application = QtWidgets.QApplication(args)  # Unreadable in 3ds max, don't forget to comment before compile script
        self.main_window = MainWindow(self)
        self.is_valid_file_path = None

        self.mocap_manager = None
        self.asset_objects_storage = None

        self.populate_import_mode_list()

    def run(self):
        self.update_asset_list()

        self.main_window.show()

        # return self.pyside_application.exec_()  # Unreadable in 3ds max, don't forget to comment before compile script

    def populate_import_mode_list(self):
        """Populate import mode combobox in view"""
        import_mode_list = [import_mode_name for import_mode_name, _ in MOCAP_TABLE.items()]
        self.main_window.populate_import_mode_list(import_mode_list=import_mode_list)

    def event_about(self):
        """Event to open 'about' view."""
        copyright_string = u'\u00A9'

        text = 'Legal information\n\n'
        text += 'Created by Julian Dropsit and Anthony Mondelin\n\n'
        text += copyright_string + ' All Rights Reserved\n\n'
        text += f'{self.name}\n'
        text += f'v{self.version}\n'
        text += f'Last released: {self.last_released}'

        message_box = MessageBox(title='ABOUT', text=text)
        message_box.show()

    def event_working(self):
        """Event to open working dir when user clicked on label."""
        user_inputs = self.main_window.user_inputs(user_inputs_class=UserInput)

        if file_path.is_valid(file_path=user_inputs.fbx_file_path):
            os.startfile(os.path.dirname(user_inputs.fbx_file_path))

    def event_path_edit(self):
        """Event when path edit is updated."""
        user_inputs = self.main_window.user_inputs(user_inputs_class=UserInput)

        self.set_label_working_file(path=user_inputs.fbx_file_path)

    def event_open(self):
        """Event to open file explorer to find fbx file in computer."""
        user_inputs = self.main_window.user_inputs(user_inputs_class=UserInput)

        last_directory = user_inputs.fbx_file_path
        default_directory = os.path.join(os.environ['USERPROFILE'], 'Desktop')

        input_file_path = self.main_window.open_file_dialog(
            directory=last_directory if last_directory else default_directory,
            filter_type='FBX files (*.fbx)'
        )

        self.set_label_working_file(input_file_path)

    def set_label_working_file(self, path: str):
        """Set label working file"""
        self.main_window.set_line_edit_file_path(text=path)

        if file_path.is_path_max_size(path):
            self.is_valid_file_path = False
            self.main_window.set_label_working_file(text='FBX FOLDER - Warning! Path is too long!', color='color: red')

        elif not file_path.is_valid(path):
            self.is_valid_file_path = False
            self.main_window.set_label_working_file(text='FBX FOLDER - THE PATH DOESN\'T EXIST.', color='color: red')

        else:
            self.is_valid_file_path = True
            self.main_window.set_label_working_file(text='FBX FOLDER', color='color: green')

    def update_asset_list(self):
        """Update assets list combobox in view"""
        dcc_objects = ExternalSingleton().dcc.return_scene_objects(asset_object=Asset)

        roots = [
            ASSETS_DISPLAY.format(name=obj.asset_name, occurrence=obj.occurrence) for obj in assets_roots.get_scene_roots(dcc_objects)
        ]

        self.main_window.populate_assets_list(roots)

    def update_time_range(self):
        """Update time range label when user inputs changes."""
        user_inputs = self.main_window.user_inputs(user_inputs_class=UserInput)

        time_to_display = ''
        if user_inputs.frame_range_mode == 'CUSTOM RANGE':
            frame_range_object = frame_range.get_frame_range(
                frame_range_mode=user_inputs.frame_range_mode,
                first_frame=user_inputs.range_from,
                last_frame=user_inputs.range_to,
                custom_start=user_inputs.custom_start if user_inputs.custom_start_is_checked else None
            )

            time_to_display = f'({frame_range_object.first}-{frame_range_object.last})'

        else:
            if user_inputs.custom_start_is_checked:
                time_to_display = f'Automatic (+{user_inputs.custom_start})'
            else:
                time_to_display = 'Automatic'

        self.main_window.set_record_time(time_to_display)

    def set_mocap_manager(self, import_mode: str):
        """Store mocap manager into application's variable."""
        if not import_mode:
            raise KeyError('No import mode selected.')

        self.mocap_manager = MOCAP_TABLE.get(import_mode)

    def _get_fbx_frame_offset(self):
        """Find and return offset tu use to import fbx keyframes."""
        user_inputs = self.main_window.user_inputs(user_inputs_class=UserInput)
        frame_offset = 0
        if user_inputs.custom_start_is_checked:
            frame_offset = user_inputs.custom_start
            if user_inputs.range_from:
                frame_offset = -(user_inputs.range_from - user_inputs.custom_start)
        return frame_offset

    @error_decorator
    def event_run(self):
        """Main method to run mocapy."""
        user_inputs = self.main_window.user_inputs(user_inputs_class=UserInput)

        selected_asset = user_inputs.asset
        if not selected_asset:
            raise TypeError('No asset selected.')

        asset_name = selected_asset.split('_')[0]
        asset_occurrence = selected_asset.split('_')[1]

        fbx_file_path = user_inputs.fbx_file_path
        if not fbx_file_path:
            raise FileNotFoundError('No FBX file selected.')

        if not file_path.is_valid(fbx_file_path):
            raise FileNotFoundError('This FBX file path doesn\'t exist.')

        self.set_mocap_manager(import_mode=user_inputs.import_mode)

        self.asset_objects_storage = store_mocap_objects.run(
            asset_name=asset_name,
            asset_occurrence=asset_occurrence,
            dcc_objects=ExternalSingleton().dcc.return_scene_objects(asset_object=Asset),
            mocap_manager=self.mocap_manager,
            dcc_objects_storage=Storage()
        )
        self.asset_objects_storage.sort_controller_reference_objects()

        references_objects_pattern = self.mocap_manager.references_regex.format(
            asset_name=asset_name,
            asset_occurrence=asset_occurrence
        )

        references_mocap_object_pattern = self.mocap_manager.mesh_reference_regex.format(
            asset_name=asset_name,
            asset_occurrence=asset_occurrence
        )

        import_animation.import_fbx(
            objects_references=self.asset_objects_storage.mocap_reference_objects,
            references_objects_pattern=references_objects_pattern,
            mocap_mesh_objects=self.asset_objects_storage.mocap_mesh_objects,
            references_mocap_object_pattern=references_mocap_object_pattern,
            fbx_file_path=fbx_file_path,
            frame_offset=self._get_fbx_frame_offset()
        )

        frame_range_object = frame_range.get_frame_range(
            frame_range_mode=user_inputs.frame_range_mode,
            first_frame=user_inputs.range_from,
            last_frame=user_inputs.range_to,
            custom_start=user_inputs.custom_start if user_inputs.custom_start_is_checked else None,
            fbx_objects=self.asset_objects_storage.mocap_reference_objects
        )

        if frame_range_object.first == frame_range_object.last:
            raise ValueError('Start frame and end frame can\'t be the same.')

        controllers_objects_pattern = self.mocap_manager.controllers_regex.format(
            asset_name=asset_name,
            asset_occurrence=asset_occurrence
        )

        transfer_animation.run(
            first_frame=frame_range_object.first,
            last_frame=frame_range_object.last,
            references_objects=self.asset_objects_storage.controller_reference_objects,
            re_pattern=controllers_objects_pattern
        )

    @error_decorator
    def event_export(self):
        """Method to export mocap rig."""
        user_inputs = self.main_window.user_inputs(user_inputs_class=UserInput)

        selected_asset = user_inputs.asset
        if not selected_asset:
            raise TypeError('No asset selected.')

        asset_name = selected_asset.split('_')[0]
        asset_occurrence = selected_asset.split('_')[1]

        self.set_mocap_manager(import_mode=user_inputs.import_mode)

        self.asset_objects_storage = store_mocap_objects.run(
            asset_name=asset_name,
            asset_occurrence=asset_occurrence,
            dcc_objects=ExternalSingleton().dcc.return_scene_objects(asset_object=Asset),
            mocap_manager=self.mocap_manager,
            dcc_objects_storage=Storage()
        )

        if not self.asset_objects_storage.mocap_reference_objects:
            raise ValueError('No mocap references in scene. Don\'t forget to generate mocap rig.')

        references_objects_pattern = self.mocap_manager.references_regex.format(
            asset_name=asset_name,
            asset_occurrence=asset_occurrence
        )

        references_mocap_object_pattern = self.mocap_manager.mesh_reference_regex.format(
            asset_name=asset_name,
            asset_occurrence=asset_occurrence
        )

        current_dir_path = ExternalSingleton().dcc.get_dir_path()
        mocap_rig_file_name = f'{asset_name}_{self.mocap_manager.name}.{ExternalSingleton().dcc.extension}'
        mocap_rig_file_path = os.path.join(current_dir_path, mocap_rig_file_name).replace('\\', '/')

        mocap_fbx_file_name = f'{asset_name}_{self.mocap_manager.name}.fbx'
        mocap_fbx_file_path = os.path.join(current_dir_path, mocap_fbx_file_name).replace('\\', '/')

        mocap_rig.export(
            scene_objects=ExternalSingleton().dcc.return_scene_objects(asset_object=Asset),
            objects_references=self.asset_objects_storage.mocap_reference_objects,
            references_objects_pattern=references_objects_pattern,
            mocap_mesh_objects=self.asset_objects_storage.mocap_mesh_objects,
            references_mocap_object_pattern=references_mocap_object_pattern,
            rig_file_path=mocap_rig_file_path,
            fbx_file_path=mocap_fbx_file_path
        )

        message_box = MessageBox(title='Done', text=f'Exported at "{current_dir_path}"')
        message_box.show()
