# Spécificités de Mocapy

## Fonction principale

**Mocapy** est un outils dédié à l'**animation** et sert à transférer 
une mocap *(fichier .fbx)* sur un asset issu de l'autorig.

## Résumé technique

Avant de lancer le script, il faut bien vérifier à avoir dans la scène les objets de référence 
pour procéder au transfert d'animation.

#### Nomenclature des objets:

> **MOBU**
> 
> | Mocap Reference | Controller Reference |
> | --- | --- |
> |nemo:Head_001 | 032#nemo:align_nemo_C_head_001 |


> **MIXAMO**
> 
> | Mocap Reference | Controller Reference |
> | --- | --- |
> | nemo_mixamorig:Head_001 | 032#mixamoRig:align_nemo_C_head_001 |


> **XSENSE**
> 
> | Mocap Reference | Controller Reference |
> | --- | --- |
> | nemo_xSenserig:Head_001 | 032#xSenseRig:align_nemo_C_head_001 |


##### Etapes:

- **Stockage** des objets de références nécessaires:
  - **Mocap Reference** pour l'import de l'animation _(par exemple: `nemo:Head_001`)_
  - **Controller Reference** pour le transfert d'animation de la référence au contrôleur d'animation du rig 
    _(par exemple: `032#nemo:align_nemo_C_head_001`)_

- **Renommage** des objets de Mocap Reference pour en supprimer le nom de l'asset et son occurrence
  - _Par exemple: `nemo:Head_001` sera renommé en `Head`_
    
- **Suppression** d'une potentielle ancienne animation sur les objets de Mocap Reference

- **Import** du fichier .fbx contenant l'animation. 
  - Les objets du .fbx portent le même nom que les objets renommés à la première étape.
  - Dans ce cas, seulement l'animation sera importée sur les objets références déjà existants de la scène.

- **Renommage** des objets de Mocap Reference pour remettre leur nom d'origine
  - _Par exemple: `Head` sera renommé en `nemo:Head_001`_

- **Déplacement** des keyframes des objets de Mocap Reference en fonction du custom start choisi.

- **Transfert** d'animation, en autokey frame par frame, de l'objet `Controller Reference` vers le contrôleur d'animation
  du rig
  - _Par exemple: le contrôleur `nemo_C_head_001` est placé au même endroit que `032#nemo:align_nemo_C_head_001`_
