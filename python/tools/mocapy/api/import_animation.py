from tools.mocapy.infrastructure.ports import ExternalSingleton
from tools.mocapy.use_cases import dcc_references, manage_keyframes


def import_fbx(
        objects_references: list,
        references_objects_pattern: str,
        mocap_mesh_objects: list,
        references_mocap_object_pattern: str,
        fbx_file_path: str, frame_offset: int
):
    """Delete keyframes already exists on mocap objects references. Rename them and import fbx file in scene.
    Restore old names and offset keyframes if needed."""
    manage_keyframes.delete_keyframes(objects_references)

    dcc_references.set_names(objects_references, references_objects_pattern)
    dcc_references.set_names(mocap_mesh_objects, references_mocap_object_pattern)

    ExternalSingleton().dcc.import_fbx(file_path=fbx_file_path)

    dcc_references.restore_names(objects_references)
    dcc_references.restore_names(mocap_mesh_objects)

    if frame_offset:
        manage_keyframes.offset_keyframes(objects_references, frame_offset)
