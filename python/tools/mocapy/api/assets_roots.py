import re


ROOT_PATTERN = re.compile('_C_Root_')


def get_scene_roots(dcc_objects: list) -> list:
    """Parse given objects then return objects with names match with pattern (_C_Root_)."""
    assets_roots = []
    for dcc_object in dcc_objects:
        if not re.search(ROOT_PATTERN, dcc_object.full_name):
            continue

        assets_roots.append(dcc_object)

    return assets_roots
