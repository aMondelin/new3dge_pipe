from tools.message_box import MessageBox
from tools.mocapy.infrastructure.ports import ExternalSingleton
from tools.mocapy.use_cases import dcc_controllers
from tools.progress_bar import ProgressUi


def run(first_frame: int, last_frame: int, references_objects: list, re_pattern: str):
    """Transfer animation from references objects to autorig's controllers from first frame to last frame."""
    ExternalSingleton().dcc.disable_viewport()
    ExternalSingleton().dcc.set_animation_range(first_frame, last_frame)
    ExternalSingleton().dcc.set_auto_key(True)

    ProgressUi.close_existing_window()
    progress_ui = ProgressUi()
    progress_ui.show()
    progress_ui.set_progress_max(last_frame - first_frame)
    progress_ui.set_window_title('Recording...')

    for frame in range(first_frame, last_frame+1):
        if progress_ui.cancelled:
            ExternalSingleton().dcc.enable_viewport()
            ExternalSingleton().dcc.set_auto_key(False)

            message_box = MessageBox(title='CANCEL', text='Cancelled.')
            message_box.show()
            return

        ExternalSingleton().dcc.set_current_frame(frame)

        for obj in references_objects:
            controller_name = dcc_controllers.get_name(obj.full_name, re_pattern)
            controller_object = ExternalSingleton().dcc.get_dcc_object(controller_name)
            if not controller_object:
                continue

            reference_transform = ExternalSingleton().dcc.get_object_transform(obj.dcc_object)
            ExternalSingleton().dcc.set_object_transform(controller_object, reference_transform)

        progress_ui.set_progress(display_step=f'{str(progress_ui.progress_value)}/{str(progress_ui.progress_max_value)}')

    progress_ui.set_progress_label('Done.')
    progress_ui.set_progress_color_end()

    ExternalSingleton().dcc.enable_viewport()
    ExternalSingleton().dcc.set_auto_key(False)
