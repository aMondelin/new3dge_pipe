import os


PATH_MAX_SIZE = 125


def is_valid(file_path: str) -> bool:
    """Check if given file path exist and return the result"""
    return os.path.exists(file_path)


def is_path_max_size(file_path: str) -> bool:
    """Check if given file path is under maximum limit and return the result"""
    return len(file_path) >= PATH_MAX_SIZE
