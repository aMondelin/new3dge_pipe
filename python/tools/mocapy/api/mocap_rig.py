from tools.mocapy.infrastructure.ports import ExternalSingleton
from tools.mocapy.use_cases import dcc_layers, dcc_references, delete_dcc_objects
from tools.progress_bar import ProgressUi


def export(
        scene_objects: list,
        objects_references: list,
        references_objects_pattern: str,
        mocap_mesh_objects: list,
        references_mocap_object_pattern: str,
        rig_file_path: str,
        fbx_file_path: str
):
    """Export mocap reference objects from autorig into new file.
    Steps:
    - Move mocap reference objects into default layer
    - Rename them
    - Delete useless objects
    - Delete empty layers
    - Save file"""
    progress_max = 8

    ProgressUi.close_existing_window()
    progress_ui = ProgressUi()
    progress_ui.show()
    progress_ui.set_progress_max(progress_max)
    progress_ui.set_window_title('Exporting...')

    progress_ui.set_progress(display_step=f'{str(progress_ui.progress_value)}/{str(progress_max)}')
    scene_objects = [scene_object.dcc_object for scene_object in scene_objects]
    objects_to_export = [object_reference.dcc_object for object_reference in objects_references]
    mesh_objects = [mocap_mesh_object.dcc_object for mocap_mesh_object in mocap_mesh_objects]
    objects_to_export += mesh_objects

    progress_ui.set_progress(display_step=f'{str(progress_ui.progress_value)}/{str(progress_max)}')
    ExternalSingleton().dcc.hold_scene()

    progress_ui.set_progress(display_step=f'{str(progress_ui.progress_value)}/{str(progress_max)}')
    ExternalSingleton().dcc.move_objects_to_default_layer(objects_to_export)

    progress_ui.set_progress(display_step=f'{str(progress_ui.progress_value)}/{str(progress_max)}')
    dcc_references.set_names(objects_references, references_objects_pattern)
    dcc_references.set_names(mocap_mesh_objects, references_mocap_object_pattern)

    progress_ui.set_progress(display_step=f'{str(progress_ui.progress_value)}/{str(progress_max)}')
    delete_dcc_objects.delete_objects(dcc_objects=scene_objects, exceptions=objects_to_export)

    progress_ui.set_progress(display_step=f'{str(progress_ui.progress_value)}/{str(progress_max)}')
    dcc_layers.delete_empty_layers(dcc_layers_count=ExternalSingleton().dcc.layer_count())

    progress_ui.set_progress(display_step=f'{str(progress_ui.progress_value)}/{str(progress_max)}')
    ExternalSingleton().dcc.save_file(file_path=rig_file_path)
    ExternalSingleton().dcc.export_fbx(file_path=fbx_file_path)

    progress_ui.set_progress(display_step=f'{str(progress_ui.progress_value)}/{str(progress_max)}')
    ExternalSingleton().dcc.fetch_scene()

    progress_ui.set_progress_label('Done.')
    progress_ui.set_progress_color_end()
