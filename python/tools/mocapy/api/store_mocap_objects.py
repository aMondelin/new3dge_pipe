import re


def _is_regex_match(pattern: str, test_name: str):
    """Check if object name fit with given pattern and return the result."""
    regex_compile = re.compile(pattern)
    if regex_compile.match(test_name):
        return True


def run(asset_name: str, asset_occurrence: str, dcc_objects: list, mocap_manager, dcc_objects_storage):
    """Parse scene objects and store them into Storage Object.
    Store them into 3 different variables, reference objects, controllers objects and mesh reference."""
    for dcc_object in dcc_objects:
        if _is_regex_match(
                pattern=mocap_manager.references_regex.format(asset_name=asset_name, asset_occurrence=asset_occurrence),
                test_name=dcc_object.full_name
        ):
            dcc_objects_storage.mocap_reference_objects.append(dcc_object)

        if _is_regex_match(
                pattern=mocap_manager.controllers_regex.format(asset_name=asset_name, asset_occurrence=asset_occurrence),
                test_name=dcc_object.full_name
        ):
            dcc_objects_storage.controller_reference_objects.append(dcc_object)

        if _is_regex_match(
                pattern=mocap_manager.mesh_reference_regex.format(asset_name=asset_name, asset_occurrence=asset_occurrence),
                test_name=dcc_object.full_name
        ):
            dcc_objects_storage.mocap_mesh_objects.append(dcc_object)

    return dcc_objects_storage
