from tools.mocapy.infrastructure.ports import ExternalSingleton, FrameRange


def get_fbx_frame_range(fbx_objects: list) -> FrameRange:
    """Get keyframes on given objets then return a FrameRange object with first frame and last frame"""
    frame_range_fbx = ExternalSingleton().dcc.get_objects_keyframes(fbx_objects)

    if frame_range_fbx:
        return FrameRange(
            first=frame_range_fbx[0],
            last=frame_range_fbx[-1]
        )

    else:
        return FrameRange()


def get_frame_range(frame_range_mode: str, first_frame: int, last_frame: int, custom_start: int = None, fbx_objects: list = []) -> FrameRange:
    """Return FrameRange object from user inputs depends on import mode:
    - Automatic > from imported fbx
    - Manual > from user inputs
    Adapt it with custom start if selected then return created FramRange."""
    if frame_range_mode == 'FILE RANGE':
        frame_range_object = get_fbx_frame_range(fbx_objects=fbx_objects)

        if frame_range_object.first == frame_range_object.last:
            raise ValueError('No animation on fbx (or wrong objects names.)')

    elif frame_range_mode == 'CUSTOM RANGE':
        frame_range_object = FrameRange(
            first=first_frame,
            last=last_frame
        )

    if custom_start is not None:
        frame_range_object = set_custom_start_frame_range(
            frame_range=frame_range_object,
            custom_start=custom_start
        )

    return frame_range_object


def set_custom_start_frame_range(frame_range: FrameRange, custom_start: int) -> FrameRange:
    """Change frame range depends on given custom start then return it."""
    time_range = frame_range.last - frame_range.first

    frame_range.first = custom_start
    frame_range.last = custom_start + time_range

    return frame_range
