#!/usr/bin/env python
# -*- coding: utf-8 -*-


import logging
import sys

from pipe.infrastructure.dcc.max_ import Max
from tools.mocapy.application import MocapyApplication
from tools.mocapy.infrastructure.ports import ExternalSingleton


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    ExternalSingleton().dcc = Max()

    application = MocapyApplication(sys.argv)
    sys.exit(application.run())
