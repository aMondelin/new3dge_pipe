from PySide2.QtCore import Qt
from PySide2.QtWidgets import QApplication, QButtonGroup, QCheckBox, QComboBox, QFileDialog, QGroupBox, QLabel, \
    QLineEdit, QPushButton, QRadioButton, QSpinBox, QVBoxLayout, QWidget


class MainWindow(QWidget):

    def __init__(self, application, parent=None):
        QWidget.__init__(self, parent)

        self.application = application

        self.init_ui()

    @staticmethod
    def close_existing_window():
        widgets = QApplication.topLevelWidgets()
        for widget in widgets:
            if widget.inherits('MainWindow'):
                widget.close()

    def init_ui(self):
        self.setFixedSize(672, 285)
        self.setWindowTitle('{} V{}'.format(self.application.name, self.application.version))

        # ABOUT
        self.qbutton_about = QPushButton('ABOUT', self)
        self.qbutton_about.setFlat(True)
        self.qbutton_about.setGeometry(630, 4, 40, 16)
        self.qbutton_about.setStyleSheet('font:6pt')

        # WORKING FOLDER
        self.qbutton_working = QPushButton('FBX FOLDER', self)
        self.qbutton_working.setFlat(True)
        self.qbutton_working.setToolTip('Chemin du FBX')
        self.qbutton_working.setGeometry(14, 33, 225, 18)
        self.qbutton_working.setStyleSheet('color: red; text-align: left;')
        self.qlineedit_dir_path = QLineEdit(self)
        self.qlineedit_dir_path.setToolTip('Chemin du FBX')
        self.qlineedit_dir_path.setFocusPolicy(Qt.ClickFocus)
        self.qlineedit_dir_path.setGeometry(16, 59, 640, 19)
        self.qbutton_open = QPushButton('OPEN...', self)
        self.qbutton_open.setToolTip('Sélectionne un fbx...')
        self.qbutton_open.setGeometry(566, 33, 90, 18)

        # SETUP
        self.qgroup_time = QGroupBox('SETUP', self)
        self.qgroup_time.setGeometry(77, 91, 229, 52)
        self.qcombo_select_scene_asset = QComboBox(self.qgroup_time)
        self.qcombo_select_scene_asset.setGeometry(15, 20, 130, 20)
        self.qcombo_select_scene_asset.setToolTip('Assets présents dans la scène.')
        self.qbutton_refresh_scene_asset = QPushButton('REFRESH', self.qgroup_time)
        self.qbutton_refresh_scene_asset.setGeometry(150, 20, 57, 20)
        self.qbutton_refresh_scene_asset.setToolTip('Update la liste des assets de la scène.')

        # IMPORT
        self.qgroup_import = QGroupBox('IMPORT MODE', self)
        self.qgroup_import.setGeometry(77, 152, 229, 69)
        self.qcombo_import_mode = QComboBox(self.qgroup_import)
        self.qcombo_import_mode.setGeometry(15, 28, 190, 20)
        self.qcombo_import_mode.setToolTip('Sélectionner le style de mocap à importer.')

        # FILE
        self.qgroup_time = QGroupBox('FILE', self)
        self.qgroup_time.setGeometry(362, 91, 229, 130)

        self.qwidget_timeline = QWidget(self.qgroup_time)
        self.qwidget_timeline.move(22, 20)
        self.qlayout_timeline = QVBoxLayout(self.qwidget_timeline)
        self.qradio_timeline_range = QRadioButton('FILE RANGE')
        self.qradio_timeline_range.setMinimumWidth(250)
        self.qradio_timeline_range.setToolTip('Utilise le range du FBX.')
        self.qradio_timeline_range.setChecked(True)
        self.qlayout_timeline.addWidget(self.qradio_timeline_range)
        self.qradio_custom_range = QRadioButton('CUSTOM RANGE')
        self.qradio_custom_range.setToolTip('Utilise un range personnalisé. Définir le debut et la fin de l\'animation à copier.')
        self.qlayout_timeline.addWidget(self.qradio_custom_range)

        self.qwidget_custom_range = QWidget(self.qgroup_time)
        self.qwidget_custom_range.move(0, 65)
        self.qwidget_custom_range.setEnabled(False)
        self.qlabel_range_from = QLabel('FROM:', self.qwidget_custom_range)
        self.qlabel_range_from.setGeometry(42, 0, 45, 16)
        self.qspinner_range_from = QSpinBox(self.qwidget_custom_range)
        self.qspinner_range_from.setGeometry(79, 0, 45, 16)
        self.qspinner_range_from.setRange(-999999, 999999)
        self.qlabel_range_to = QLabel('TO:', self.qwidget_custom_range)
        self.qlabel_range_to.setGeometry(133, 0, 45, 16)
        self.qspinner_range_to = QSpinBox(self.qwidget_custom_range)
        self.qspinner_range_to.setGeometry(154, 0, 45, 16)
        self.qspinner_range_to.setRange(-999999, 999999)

        self.qradio_custom_start = QCheckBox('CUSTOM START AT:', self.qgroup_time)
        self.qradio_custom_start.move(24, 95)
        self.qradio_custom_start.setToolTip('Permet d\'ajouter un offset à l\'animation.')
        self.qspinner_custom_start = QSpinBox(self.qgroup_time)
        self.qspinner_custom_start.setGeometry(143, 96, 45, 16)
        self.qspinner_custom_start.setRange(-999999, 999999)
        self.qspinner_custom_start.setEnabled(False)

        self.qbuttongroup_time = QButtonGroup()
        self.qbuttongroup_time.addButton(self.qradio_timeline_range)
        self.qbuttongroup_time.addButton(self.qradio_custom_range)

        # RUN
        self.qlabel_record_time = QLabel('Automatic', self)
        self.qlabel_record_time.setToolTip('Frame Range')
        self.qlabel_record_time.setGeometry(259, 230, 150, 13)
        self.qlabel_record_time.setAlignment(Qt.AlignCenter)
        self.qbutton_run_script = QPushButton('LOAD', self)
        self.qbutton_run_script.setGeometry(259, 244, 150, 30)
        self.qbutton_run_script.setToolTip('Go!')
        self.qbutton_export = QPushButton('E', self)
        self.qbutton_export.setGeometry(412, 244, 15, 30)
        self.qbutton_export.setToolTip('Permet d\'exporter le setup pour le mode sélectionné.')

        # EVENTS
        self.qbutton_about.clicked.connect(self.application.event_about)
        self.qbutton_working.clicked.connect(self.application.event_working)
        self.qlineedit_dir_path.editingFinished.connect(self.application.event_path_edit)
        self.qbutton_open.clicked.connect(self.application.event_open)

        self.qbutton_refresh_scene_asset.clicked.connect(self.application.update_asset_list)
        self.qradio_timeline_range.toggled.connect(self.toggle_time_range)
        self.qspinner_range_from.valueChanged.connect(self.application.update_time_range)
        self.qspinner_range_to.valueChanged.connect(self.application.update_time_range)
        self.qradio_custom_start.toggled.connect(self.toggle_time_range)
        self.qspinner_custom_start.valueChanged.connect(self.application.update_time_range)

        self.qbutton_run_script.clicked.connect(self.application.event_run)
        self.qbutton_export.clicked.connect(self.application.event_export)

    def user_inputs(self, user_inputs_class):
        return user_inputs_class(
            fbx_file_path=self.qlineedit_dir_path.text(),
            asset=self.qcombo_select_scene_asset.currentText(),
            import_mode=self.qcombo_import_mode.currentText(),
            frame_range_mode=self.qbuttongroup_time.checkedButton().text(),
            range_from=self.qspinner_range_from.value(),
            range_to=self.qspinner_range_to.value(),
            custom_start_is_checked=self.qradio_custom_start.isChecked(),
            custom_start=self.qspinner_custom_start.value(),
        )

    def set_record_time(self, text: str):
        self.qlabel_record_time.setText(text)

    def toggle_time_range(self):
        self.qwidget_custom_range.setEnabled(
            self.qradio_custom_range.isChecked() and self.qradio_custom_range.isEnabled()
        )
        self.qspinner_custom_start.setEnabled(self.qradio_custom_start.isChecked())
        self.application.update_time_range()

    def open_file_dialog(self, directory: str, filter_type: str):
        input_file_path, _ = QFileDialog().getOpenFileName(self, 'Open File', directory, filter_type)

        return input_file_path

    def set_label_working_file(self, text: str = None, color: str = None):
        if text:
            self.qbutton_working.setText(text)
        if color:
            self.qbutton_working.setStyleSheet(f'text-align: left; {color}')

    def set_line_edit_file_path(self, text: str):
        self.qlineedit_dir_path.setText(text)

    def populate_assets_list(self, assets_list: list):
        self.qcombo_select_scene_asset.clear()
        self.qcombo_select_scene_asset.addItems(assets_list)

    def populate_import_mode_list(self, import_mode_list: list):
        self.qcombo_import_mode.clear()
        self.qcombo_import_mode.addItems(import_mode_list)
