from tools.mocapy.infrastructure.ports import ExternalSingleton


def offset_keyframes(objects_references: list, frame_offset: int):
    """Move keyframes on given objects by frame offset."""
    for object_reference in objects_references:
        ExternalSingleton().dcc.offset_keyframes(object_reference.dcc_object, frame_offset)


def delete_keyframes(objects_references: list):
    """Delete keyframes on given objects."""
    for object_reference in objects_references:
        ExternalSingleton().dcc.delete_keyframes(object_reference.dcc_object)
