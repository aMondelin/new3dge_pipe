from tools.mocapy.infrastructure.ports import ExternalSingleton


def delete_empty_layers(dcc_layers_count):
    """Delete all dcc's empty layers."""
    ExternalSingleton().dcc.set_default_layer_active()

    for dcc_layer_index in reversed(range(dcc_layers_count)):
        if ExternalSingleton().dcc.is_default_layer(dcc_layer_index):
            continue

        if ExternalSingleton().dcc.is_empty_layer(dcc_layer_index):
            ExternalSingleton().dcc.delete_layer(dcc_layer_index)
