import re


def get_name(object_name: str, re_pattern: str):
    """Return autorig's controller name from mocap reference name."""
    pattern = re.compile(re_pattern)

    controller_name = re.match(pattern, object_name).group(1)
    return controller_name
