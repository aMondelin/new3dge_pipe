import re

from tools.mocapy.infrastructure.ports import ExternalSingleton


def set_names(objects_references: list, re_pattern: str):
    """Rename autorig's mocap references objects to fit with fbx objects' names."""
    pattern = re.compile(re_pattern)

    for obj in objects_references:
        new_object_name = re.match(pattern, obj.full_name).group(1)
        ExternalSingleton().dcc.rename_object(obj.dcc_object, new_object_name)


def restore_names(objects_references: list):
    """Restore autorig's mocap references objects names from stored names."""
    for obj in objects_references:
        ExternalSingleton().dcc.rename_object(obj.dcc_object, obj.full_name)
