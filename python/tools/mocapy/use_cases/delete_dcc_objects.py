from tools.mocapy.infrastructure.ports import ExternalSingleton


def delete_objects(dcc_objects: list, exceptions: list = []):
    """Delete given dcc objects."""
    for dcc_object in dcc_objects:
        if dcc_object in exceptions:
            continue

        ExternalSingleton().dcc.delete_object(dcc_object)
