

class Asset:
    separator = '_'

    def __init__(self, object_name: str, dcc_object):
        self.full_name = object_name
        self.asset_name = None
        self.occurrence = None
        self.dcc_object = dcc_object

        self.split_object_name(object_name)

    def split_object_name(self, object_name: str):
        split_name = object_name.split(self.separator)
        self.asset_name = split_name[0]
        self.occurrence = split_name[-1]

    def __lt__(self, other):
        return self.full_name < other.full_name

    def __repr__(self):
        return f'<Asset(' \
               f'full_name="{self.full_name}", ' \
               f'asset_name="{self.asset_name}", ' \
               f'occurrence="{self.occurrence}")>'
