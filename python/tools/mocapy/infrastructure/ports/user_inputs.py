from dataclasses import dataclass


@dataclass
class UserInput:
    fbx_file_path: str = ''
    asset: str = ''
    import_mode: str = ''
    frame_range_mode: str = ''
    range_from: int = 0
    range_to: int = 0
    custom_start_is_checked: str = ''
    custom_start: int = 0
