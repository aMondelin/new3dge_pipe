from dataclasses import dataclass


@dataclass
class FrameRange:
    first: int = 0
    last: int = 0

    def __repr__(self):
        return f'<FrameRange(first_frame="{self.first}", last_frame="{self.last}")>'
