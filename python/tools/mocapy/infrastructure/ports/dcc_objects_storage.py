from dataclasses import dataclass, field


@dataclass
class Storage:
    mocap_reference_objects: list = field(default_factory=lambda: [])
    controller_reference_objects: list = field(default_factory=lambda: [])
    mocap_mesh_objects: list = field(default_factory=lambda: [])

    def sort_controller_reference_objects(self):
        self.controller_reference_objects.sort()
