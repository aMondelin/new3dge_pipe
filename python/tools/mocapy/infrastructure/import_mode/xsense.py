

class Xsense:
    name = 'xsense'

    references_regex = '^{asset_name}_(xSenserig:(.*?))_{asset_occurrence}$'
    controllers_regex = '^\d{{3}}#xSenseRig:align_({asset_name}_(.*?)_{asset_occurrence})'
    mesh_reference_regex = '^{asset_name}_(xSenseRig_fullbody_sd0)_{asset_occurrence}$'
