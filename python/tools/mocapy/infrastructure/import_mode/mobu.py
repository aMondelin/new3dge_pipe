

class Mobu:
    name = 'mobu'

    references_regex = '^{asset_name}:((.*?))_{asset_occurrence}$'
    controllers_regex = '^\d{{3}}#{asset_name}:align_({asset_name}_(.*?)_{asset_occurrence})'
    mesh_reference_regex = '^{asset_name}_(MobuRig_fullbody_sd0)_{asset_occurrence}$'
