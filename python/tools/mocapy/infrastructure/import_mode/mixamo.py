

class Mixamo:
    name = 'mixamo'

    references_regex = '^{asset_name}_(mixamorig:(.*?))_{asset_occurrence}$'
    controllers_regex = '^\d{{3}}#mixamoRig:align_({asset_name}_(.*?)_{asset_occurrence})'
    mesh_reference_regex = '^{asset_name}_(MixamoRig_fullbody_sd0)_{asset_occurrence}$'
