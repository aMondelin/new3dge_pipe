DestroyDialog rigTools
DestroyDialog CreateTreads
DestroyDialog ParentSpaceCreator
DestroyDialog AutoWheelingTool


VERSION = "1.2.0"


rollout CreateTreads "Create Treads" width:175 height:80
(
	spinner sp_treads_count "Treads Count" pos:[26, 14] width:90 heigth:15 range:[1, 100, 28] type:#integer align:#left
	button button_create "CREATE" pos:[35, 40] width:100 heigth:25 align:#left
	
	on button_create pressed do
	(
		undo on
		(
			selected_splines = GetCurrentSelection()
		
			for selected_spline in selected_splines do
			(
				point_upnode = Point size:15 cross:false box:true wirecolor:green name:(selected_spline.name + "_upnode")
				point_upnode.transform = selected_spline.transform * (matrix3 [1, 0, 0] [0, 1, 0] [0, 0, 1] [0, 0, 70])
				point_upnode.parent = selected_spline
				
				treads_count = sp_treads_count.value + 1
				points_list = #()
				for tread in 1 to treads_count do
				(
					point_ = Point size:15 cross:true box:false wirecolor:green name:(selected_spline.name + "_tread0"+ tread as string)
					point_.parent = selected_spline
					append points_list point_
					
					pos_constraint = Path_Constraint()
					pos_constraint.path = selected_spline
					
					point_.position.controller = Position_List()
					point_.position.controller[2].controller = pos_constraint
					point_.position.controller.setActive 2
					
					treads_percent = 1.0 / (sp_treads_count.value as float)
					
					if tread == 1 then 
					(
						point_.size = 25
						point_.wirecolor = red
						point_.position.controller[2].controller.percent.controller = Float_List()
					)
					
					else
					(
						wire_script = "Percent + (" + (tread-1) as string + " * " + treads_percent as string + ")"
						paramWire.connect points_list[1].pos.controller.Path_Constraint.controller[#Percent] points_list[tread].pos.controller.Path_Constraint.controller[#Percent] wire_script
					)
				)
				
				for tread in 1 to treads_count do
				(
					look_at_constraint = LookAt_Constraint ()
					look_at_constraint.target_axis = 1
					look_at_constraint.lookat_vector_length = 0.0
					look_at_constraint.viewline_length_abs = off
					
					if tread == treads_count then
					(
						get_target_node = points_list[1]
					)
					else
					(
						get_target_node = points_list[tread+1]
					)
					look_at_constraint.appendTarget get_target_node 100.0
					look_at_constraint.upnode_world  = False
					look_at_constraint.pickUpNode = point_upnode
					
					points_list[tread].rotation.controller = Rotation_List()
					points_list[tread].rotation.controller[2].controller = look_at_constraint
					points_list[tread].rotation.controller.setActive 2
				)
			)
		)
	)
)


rollout ParentSpaceCreator "Parent Space Creator" width:231 height:267
(
	groupBox group_parent_space "Add Parents Space" pos:[9, 9] width:212 height:200
	combobox combo_items "" pos:[19, 30] width:191 height:9
	button button_add "Add" pos:[18, 171] width:60 height:25
	button button_remove "Remove" pos:[84, 171] width:60 height:25
	button button_move_up "^" pos:[149, 171] width:60 height:12
	button button_move_down "v" pos:[149, 184] width:60 height:12
	button button_create "Create" pos:[70, 222] width:91 height:33

	local parents_spaces_array = #()
	
	function update_combo_items selected_index:1 = 
	(
		combo_items.items = parents_spaces_array
		combo_items.selection = selected_index
	)
	
	function is_item_in_array array_items item_ =
	(
		for array_item in array_items where array_item == item_ do return True
		
		return False
	)
	
	on button_add pressed do
	(
	    user_input = combo_items.text
	    if user_input != "" and not is_item_in_array combo_items.items user_input do 
		(
			append parents_spaces_array user_input
			update_combo_items selected_index:(parents_spaces_array.count)
		)
	)
	
	on button_remove pressed do
	(
		selected_index = combo_items.selection
		if selected_index != 0 do
		(
			deleteItem parents_spaces_array selected_index
			update_combo_items selected_index:1
		)	
	)
	
	on button_move_up pressed do
	(
		selected_index = combo_items.selection
		selected_text = combo_items.selected
		if selected_index > 1 do
		(
			deleteItem parents_spaces_array selected_index
			insertItem selected_text parents_spaces_array (selected_index-1)
			update_combo_items selected_index:(selected_index-1)
		)
	)
	
	on button_move_down pressed do
	(
		selected_index = combo_items.selection
		selected_text = combo_items.selected
		if selected_index < parents_spaces_array.count do
		(
			deleteItem parents_spaces_array selected_index
			insertItem selected_text parents_spaces_array (selected_index+1)
			update_combo_items selected_index:(selected_index+1)
		)
	)
	
	on button_create pressed do
	(
		undo on 
		(
			if selection.count == 0 do return False
			
			objects_selection = selection
			for obj in objects_selection do
			(
				switch_point = Point name:"*asset*_H_*description*-switch_001" size:10 box:true cross:false wirecolor:red box:true cross:false axistripod:false centermarker:false
				switch_point.transform = obj.transform
				switch_point.parent = obj.parent
				obj.parent = switch_point
							
				ca = attributes Custom_Attributes
				(
					Parameters main rollout:params
					(
						parent_space Type:#Integer UI:dropdownlist_parent_space Default:1
						ddl_items Type:#StringTab tabSizeVariable:true
					)
					
					Rollout Params "Custom Attributes"
					(
						dropdownList dropdownlist_parent_space "Parent Space" Width:150 Height:30 Align:#Center Type:#Array Items:(ddl_items as array)
						button button_copy "Copy" Width:65 Height:20 Align:#Left Offset:[0,0] Type:#BOOLEAN
						button button_paste "Paste" Width:65 Height:20 Align:#Right Offset:[0,-25] Type:#BOOLEAN
						
						local save_transforms
						
						on button_copy pressed do save_transforms = $.transform
						on button_paste pressed do try($.transform = save_transforms) catch()
					)
				)
				
				attribute_holder = EmptyModifier()
				custAttributes.add attribute_holder ca
				attribute_holder.Custom_Attributes.ddl_items = parents_spaces_array
				
				addmodifier obj attribute_holder
				
				switch_position_constraint = position_constraint()
				switch_point.pos.controller = switch_position_constraint
				
				switch_orientation_constraint = orientation_constraint()
				switch_point.rotation.controller = switch_orientation_constraint

				for combo_index in 1 to combo_items.items.count do
				(
					point_name = "*asset*_H_*description*-" + combo_items.items[combo_index] + "_001"
					item_point = Point name:point_name size:(10+combo_index) box:true cross:false wirecolor:green box:true cross:false axistripod:false centermarker:false
					item_point.transform = obj.transform
					
					switch_position_constraint.appendTarget item_point 100
					switch_orientation_constraint.appendTarget item_point 100
					
					wire_expression = "if parent_space == " + combo_index as string + " then 100 else 0"

					paramWire.connect attribute_holder.Custom_Attributes[#parent_space] switch_position_constraint[combo_index] wire_expression
					paramWire.connect attribute_holder.Custom_Attributes[#parent_space] switch_orientation_constraint[combo_index] wire_expression
				)
			)
		)
	)
)


rollout AutoWheelingTool "Auto Wheeling Tool" width:231 height:336
(
	function valid_geometry_types obj =
	(
		return superclassof obj == GeometryClass
	)
	
	function valid_spline_types obj =
	(
		return superclassof obj == shape
	)
	
	function valid_helper_types obj =
	(
		return superclassof obj == helper
	)
	
	GroupBox 'group_input' "Input" pos:[15,12] width:202 height:263 align:#left
	label 'label_spline' "Spline" pos:[30,45] width:43 height:14 align:#left
	pickbutton 'pick_spline' "PickButton" pos:[119,36] width:84 height:34 filter:valid_spline_types align:#left
	label 'label_constraint' "Path Constraint" pos:[30,89] width:78 height:14 align:#left
	pickbutton 'pick_constraint' "PickButton" pos:[119,77] width:84 height:34 filter:valid_helper_types align:#left

	groupBox 'group_wheel' "Wheel" pos:[28,121] width:175 height:109 align:#left
	label 'label_radius' "Radius" pos:[47,142] width:49 height:14 align:#left
	spinner 'spinner_radius' "" pos:[102,141] width:85 height:16 range:[0,100,5] default:5 align:#left
	label 'label_pick_target' "Pick Target" pos:[46,166] width:59 height:15 align:#left enabled:false
	pickbutton 'pick_wheel_target' "PickButton" pos:[42,186] width:84 height:34 filter:valid_geometry_types align:#left enabled:false tooltip:"yo"
	label 'label_use' "Use" pos:[165,166] width:28 height:15 align:#left
	checkbox 'check_use_target' "" pos:[166,194] width:20 height:18 align:#left
	
	label 'label_rotation_axis' "Rotation axis" pos:[30,244] width:78 height:14 align:#left
	dropdownList 'list_rotation_axis' "" pos:[117,241] width:85 height:21 items:#("X", "Y", "Z") align:#left
	button 'button_create' "Create" pos:[57,287] width:115 height:36 align:#left
	
	on pick_spline picked obj do
	(
		if obj != undefined do
		(
			pick_spline.text = obj.name
		)
	)
	
	on pick_constraint picked obj do
	(
		if obj != undefined do
		(
			pick_constraint.text = obj.name
		)
	)
	
	on pick_wheel_target picked obj do
	(
		if obj != undefined do
		(
			pick_wheel_target.text = obj.name
		)
	)
	
	on check_use_target changed state do
	(
		label_radius.enabled = not state
		spinner_radius.enabled = not state
		
		label_pick_target.enabled = state
		pick_wheel_target.enabled = state
	)
	
	on button_create pressed do
	(
		undo on
		(
			if selection.count == 0 or pick_spline.object == undefined or pick_constraint.object == undefined do return false
			if check_use_target.checked == true and pick_wheel_target.object == undefined do return false
					
			for object_ in selection do
			(
				script = float_script()
				script.addObject "spline" pick_spline.object
				
				try
				(
					script.addTarget "percent" pick_constraint.object.pos.controller[2].percent.controller
				)
				catch
				(
					print("You need path constraint on object: " + pick_constraint.object.name)
					continue
				)
				
				if check_use_target.checked == true then
				(
					copy_wheel = copy pick_wheel_target.object name:"wheel_reference_to_keep"
					copy_wheel.baseobject = pick_wheel_target.object.baseobject
					copy_wheel.parent = undefined
					
					script.addObject "wheel" copy_wheel
					script.setExpression "((curveLength spline)*(percent/100))/((wheel.max - wheel.min).z/2)"
				)
				else
				(
					script.addConstant "wheel_radius" spinner_radius.value
					script.setExpression "((curveLength spline)*(percent/100))/wheel_radius"
				)
				
				if list_rotation_axis.selected == "X" then object_.rotation.controller.X_Rotation.controller = script
				else if list_rotation_axis.selected == "Y" then object_.rotation.controller.Y_Rotation.controller = script
				else if list_rotation_axis.selected == "Z" then object_.rotation.controller.Z_Rotation.controller = script
			)
		)
	)
)


rollout rigTools ("Rig Tools - v" + VERSION) width:190 height:564
(
	local group_pos_x = 20
	local group_width = 150
	
	local button_pos_x = 35
	local button_width = 120
	local button_height = 25
	
	GroupBox group_parent "Hierarchy" pos:[group_pos_x, 20] width:group_width height:119 align:#left
	button button_parent_last "Parent Last" pos:[button_pos_x, 40] width:button_width height:button_height align:#left
	button button_parent_inc "Parent Increment" pos:[button_pos_x, 70] width:button_width height:button_height align:#left
	button button_parent_1by1 "Parent 1 by 1" pos:[button_pos_x, 100] width:button_width height:button_height align:#left
		
	GroupBox group_makezero "Controllers" pos:[group_pos_x, 154] width:group_width height:90 align:#left
	button button_make_zero "Create Zero" pos:[button_pos_x, 174] width:button_width height:button_height align:#left
	button button_reset "Reset" pos:[button_pos_x, 204] width:button_width height:button_height align:#left
	
	GroupBox group_splines "Splines" pos:[group_pos_x, 259] width:group_width height:90 align:#left
	button button_helper_spline "Rig Spline" pos:[button_pos_x, 279] width:button_width height:button_height align:#left
	button button_create_treads "Create Treads" pos:[button_pos_x, 309] width:button_width height:button_height align:#left
	
	GroupBox group_misc "Misc" pos:[group_pos_x, 364] width:group_width height:180 align:#left
	button button_baseobject "Change Base Object" pos:[button_pos_x, 384] width:button_width height:button_height align:#left
	button button_remove_empty_bones "Remove Empty Bones" pos:[button_pos_x, 414] width:button_width height:button_height align:#left
	button button_parent_space "Create Parent Space" pos:[button_pos_x, 444] width:button_width height:button_height align:#left
	button button_auto_wheeling "Auto Wheeling" pos:[button_pos_x, 474] width:button_width height:button_height align:#left
	button button_refresh_viewport "Refresh Viewport" pos:[button_pos_x, 504] width:button_width height:button_height align:#left
	
	function create_zero_object object_ = 
	(
		separator = "_"
		zero_type_name = "HZ"
		
		object_name_filter = filterString object_.name separator
		
		if object_name_filter.count == 4 or object_name_filter.count == 5 then
		(
			object_name_filter[2] = zero_type_name
		)
		else
		(
			append object_name_filter zero_type_name
		)
		
		zero_object_name = object_name_filter[1]
		for object_name in object_name_filter where object_name != zero_object_name do
		(
			zero_object_name += separator + object_name
		)
		
		zero_object = Point size:.1 wirecolor:green name:zero_object_name box:true cross:false axistripod:false centermarker:false
		zero_object.transform = object_.transform
		zero_object.parent = object_.parent
		object_.parent = zero_object
	)
	
	function make_zero object_:"" selected_only:False = 
	(
		if selected_only then
		(
			selected_objects = getCurrentSelection()
			if selected_objects.count != 0 do (		
				for object_ in selected_objects do (
					create_zero_object object_
				)
			)
		)
		else
		(
			if object_ != "" do 
			(
				create_zero_object object_
			)
		)
	)
	
	function look_at_matrice source_pos target_pos = 
	(
		z_vector = normalize (target_pos - source_pos)
		y_vector = normalize (cross [0, 0, 1] z_vector)
		x_vector = normalize (cross z_vector y_vector)

		return Matrix3 x_vector y_vector z_vector source_pos
	)
	
	on button_parent_last pressed do
	(
		undo on
		(
			selected_objects = getCurrentSelection()
			selected_objects_count = selected_objects.count
			
			for obj in 1 to (selected_objects_count-1) do
			(
				selected_objects[obj].parent = selected_objects[selected_objects_count]
			)
		)
	)
	
	on button_parent_inc pressed do
	(
		undo on
		(
			selected_objects = getCurrentSelection()
			selected_objects_count = selected_objects.count
			
			for obj in 1 to (selected_objects_count-1) by 2 do
			(
				selected_objects[obj].parent = selected_objects[obj+1]
			)
		)
	)
	
	on button_parent_1by1 pressed do
	(
		undo on
		(
			selected_objects = getCurrentSelection()
			selected_objects_count = selected_objects.count
			
			for obj in 1 to (selected_objects_count-1) do
			(
				selected_objects[obj].parent = selected_objects[obj+1]
			)
		)
	)
	
	on button_make_zero pressed do
	(
		undo on
		(
			make_zero selected_only:True
		)			
	)
	
	on button_reset pressed do
	(
		undo on
		(
			for controller in getCurrentSelection() do
			(
				controller_parent = controller.parent
				
				if controller_parent != undefined then
				(
					controller.transform = controller_parent.transform * matrix3 1
				)
				else
				(
					controller.transform = matrix3 1
				)
			)
		)
	)
	
	on button_helper_spline	pressed do
	(
		undo on 
		(
			selected_objects = getCurrentSelection()

			for obj in selected_objects do
			(
				try(spline_count = numSplines obj) 
				catch(
					messageBox ("You need to convert " + obj.name + " as editable spline.")
					continue
				)
				new_bones = #()

				for spline_index = 1 to spline_count do
				(
					knots_count = numKnots obj spline_index
					
					for knot_index = 1 to knots_count do
					(
						setKnotType obj spline_index knot_index #bezierCorner
						
						point_master_position = getKnotPoint obj spline_index knot_index
						point_bezier_in_position = getInVec obj spline_index knot_index
						point_bezier_out_position = getOutVec obj spline_index knot_index
						
						ctrl_master = Rectangle name:("*asset*_C_*description*-skinMstr0" + knot_index as string + "_001") transform:(look_at_matrice point_master_position point_bezier_in_position) length:20 width:40 wirecolor:yellow
						make_zero object_:ctrl_master
						point_master = Point name:("*asset*_B_*description*-skinMstr0" + knot_index as string + "_001") transform:ctrl_master.transform size:16 wirecolor:green parent:ctrl_master box:true cross:false axistripod:false centermarker:false
						append new_bones point_master
						
						ctrl_bezier_in = Rectangle name:("*asset*_C_*description*-bezierIn0" + knot_index as string + "_001") rotation:point_master.rotation position:point_bezier_in_position length:10 width:25 wirecolor:yellow parent:ctrl_master
						make_zero object_:ctrl_bezier_in
						point_bezier_in = Point name:("*asset*_B_*description*-bezierIn0" + knot_index as string + "_001") transform:ctrl_bezier_in.transform size:8 wirecolor:green parent:ctrl_bezier_in box:true cross:false axistripod:false centermarker:false
						append new_bones point_bezier_in
						
						ctrl_bezier_out = Rectangle name:("*asset*_C_*description*-bezierOut0" + knot_index as string + "_001") rotation:point_master.rotation position:point_bezier_out_position length:10 width:25 wirecolor:yellow parent:ctrl_master
						make_zero object_:ctrl_bezier_out
						point_bezier_out = Point name:("*asset*_B_*description*-bezierOut0" + knot_index as string + "_001") transform:ctrl_bezier_out.transform size:8 wirecolor:green parent:ctrl_bezier_out box:true cross:false axistripod:false centermarker:false
						append new_bones point_bezier_out
					)
				)
				
				updateShape obj
				
				skin_mod = Skin()
				addmodifier obj skin_mod
				for bone_index in 1 to new_bones.count do 
				(
					modPanel.setCurrentObject obj.modifiers[#Skin]
					
					skinOps.addbone skin_mod new_bones[bone_index] -1
					
					skinOps.SetInnerRadius skin_mod bone_index 1 .1
					skinOps.SetOuterRadius skin_mod bone_index 1 .2
					skinOps.SetInnerRadius skin_mod bone_index 2 .1
					skinOps.SetOuterRadius skin_mod bone_index 2 .2
				)
				
				skin_mod.filter_vertices = on
				skin_vertices =	skinOps.GetNumberVertices skin_mod
				skinOps.SelectVertices skin_mod #{1..skin_vertices}
				skinOps.bakeSelectedVerts skin_mod 
			)
		)			
	)

	on button_create_treads pressed do
	(
		DestroyDialog CreateTreads
		CreateDialog CreateTreads
	)
	
	on button_baseobject pressed do
	(
		undo on
		(
			selected_objects = getCurrentSelection()
			selected_objects_count = selected_objects.count
			
			for obj in 1 to (selected_objects_count-1) do
			(
				selected_objects[obj].baseobject = selected_objects[selected_objects_count].baseobject
			)
		)
	)
	
	on button_remove_empty_bones pressed do
	(
		undo on
		(
			weight_threshold = 0.0001
			selected_objects = getCurrentSelection()

			for obj in selected_objects do
			(				
				for modifier_ in obj.modifiers where classof modifier_ == Skin do 
				(
					modPanel.setCurrentObject modifier_
					
					vertex_count = skinOps.GetNumberVertices modifier_
					bones_count = skinOps.GetNumberBones modifier_
					unused_bones_index = #{1..bones_count}
					
					for vertex_ in 1 to vertex_count do (
						vertex_weight_count = skinOps.GetVertexWeightCount modifier_ vertex_
						
						for weight_id in 1 to vertex_weight_count do (
							vertex_weight = skinOps.GetVertexWeight modifier_ vertex_ weight_id
							
							if vertex_weight >= weight_threshold then (
								bone_index = skinOps.GetVertexWeightBoneID modifier_ vertex_ weight_id
								unused_bones_index[bone_index] = false
							)
						)
					)
					
					for bone_index in bones_count to 1 by -1 where unused_bones_index[bone_index] do (
						skinOps.SelectBone modifier_ bone_index
						skinOps.RemoveBone modifier_
					)
				)
			)
		)
	)
	
	on button_parent_space pressed do
	(
		DestroyDialog ParentSpaceCreator
		CreateDialog ParentSpaceCreator
	)

	on button_auto_wheeling pressed do
	(
		DestroyDialog AutoWheelingTool
		CreateDialog AutoWheelingTool
	)

	on button_refresh_viewport pressed do
	(
		enableSceneRedraw()
		redrawViews()
		completeRedraw()
	)
)


CreateDialog rigTools
